# DTS

DTS test & integration scripts

# Committing changes from Jupyter

To commit updates when running this from a station's Jupyter Notebook, do:

    ssh tangosys@station
    cd git/tango/jupyter-notebooks/dts

If you cannot login as lofarsys, ask for your public key to be added to `~tangosys/.ssh/authorized_keys`. 

Git commits require you to state your identity, as to know who did this commit. Do this manually each session:

    export GIT_AUTHOR_NAME="Your Name"
    export GIT_AUTHOR_EMAIL="you@astron.nl"
    export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
    export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"

    # add file(s) to commit list
    git add <notebook>.ipynb

    # do the commit
    git commit -m "i added this very cool notebook update"

    # push it to the server
    git push

If you don't provide your name and e-mail, ``git`` will tempt you to configure the name and email permanently. Doing so means all checkins from this system will be done under your name, however.

To make the above easier, you could put the ``export`` commands in a personal ``myname.sh`` file which you then run in your shell using ``source myname.sh`` before running ``git commit``:

    # do this once
    cat > ~/myname_creds.sh <<HEREDOC
    export GIT_AUTHOR_NAME="Your Name"
    export GIT_AUTHOR_EMAIL="you@astron.nl"
    export GIT_COMMITTER_NAME="\$GIT_AUTHOR_NAME"
    export GIT_COMMITTER_EMAIL="\$GIT_AUTHOR_EMAIL"
    HEREDOC

    # before committing just do this
    source ~/myname_creds.sh


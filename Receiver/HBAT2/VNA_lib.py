#!/usr/bin/env python
from rohdeschwarz import *
from rohdeschwarz.instruments.vna import *

def valid_ip(address):
    import socket
    try: 
        socket.inet_aton(address)
        return True
    except:
        return False
        
def init_vna(ip='10.87.6.21'):
    #"""
    #This function sets up the LXI / ethernet connection and loads all modules
    #"""
    import sys
   
    #if __name__ != "__main__":
    #    print("'{0}'\nis a script. Do not import!".format(__file__))
    #    print('Exiting...')
    #    sys.exit()

    vna = Vna()
   
    if valid_ip(ip):
       if ip == '10.87.6.21':
          vna.open_tcp('10.87.6.21', 5025)        
       else:
          vna.open_tcp(ip, 5025)
    else:  
      print('IP length is not ok')
      return 
    
    #Turn on LOG file
    vna.open_log('SCPI Command Log.txt')
    return vna
    print(vna.id_String)
    print('Init done!')


def sparam(vna,path, fn):
    #"""
    #This function measures S21 and saves it into a fname on the vna in path.
    #"""
    
    vna.channel(1).measure([1,2])
    # save the file of channel 1, where, port 1, port 2, format (re,im)
    # We use rawstrings
    raw_path = r"{}".format(path)
    raw_fn = r"{}".format(fn)    
    nm = raw_path + raw_fn
    vna.channel(1).save_measurement(nm, [2,1,])
    print(nm , "Sparam done!")

def download(vna,source,destiny):
    #"""
    #This function downloads the file from vna instrument to  the local computer
    #"""
    raw_path = r"{}".format(path)
    raw_fn = r"{}".format(fn)    
    vna.file.download_file(source,destiny)
    print("Download done!")


#Testing
#vna.channel(1).measure([1,2])
#vna.channel(1).save_measurement("nm", [2,1,])
#sparam('c:\\Users\\Public\\','j')


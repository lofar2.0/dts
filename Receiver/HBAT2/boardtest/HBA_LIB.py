#!/usr/bin/env python
# coding: utf-8
# ## Uses pi pico with l2hbat_pico program
import serial
import hbat1 as hb
from time import sleep
from datetime import datetime
import logging

HBA_X=0
HBA_Y=4
HBA_SWX=0x8
HBA_SWY=0x9
HBA_FE_PWR=0xA
HBA_PWR=0xB
HBA_ID=0x10
HBA_VERSION=0x14
HBA_ADDR=0x20
HBA_VSENSE=0x34
HBA_SAVE_EEPROM=0x40
HBA_LOAD_EEPROM=0x41
#HBA selected to speak to
def hextostr(D2): return ''.join(['{0:02x}'.format(a) for a in D2])
class HBAbf:
    def __init__ (self,port="COM5",select=15,debug=False):
        self.ser = serial.Serial(port,115200,timeout=0.1)  # open windows10 serial port      
        logging.info("connecting to: "+self.ser.name)         # check which port was really used 

        self.HBA_select=select
        self.Debug=debug

    def SetRegisters(self,reg,data):
        func=len(data)*2;
        TX1=hb.MakeRequest(self.HBA_select,data,func,reg);
        if self.Debug: print("Packet to TX",TX1)
        TX2=hb.ManchesterEncode(TX1)
        self.ser.write(bytearray(TX2))
        self.GetPackets();
#        sleep(0.2)
    def RequestRegisters(self,reg,length):
        func=length*2+1;
        TX1=hb.MakeRequest(self.HBA_select,[],func,reg);
        if self.Debug: print("Packet to TX",TX1)
        TX2=hb.ManchesterEncode(TX1)
        self.ser.write(bytearray(TX2))
        return self.GetPackets();
    def GetPackets(self):
      D=hb.GetDelay(self.ser);
      D2=hb.NormDelays(D)
      #print("Received delays:",D2[:10])
      NoData=True;
      S=hb.Decode(D2)
      RXdata=[]
#      print("Received packets:",S)
      while len(S)>0:
        NoData=False;
        L=S[1]
        S1=S[:L+3]
        CRC=hb.CRCcheck(S1);
        if self.Debug: print("Received packet:",S1,"CRC=",CRC)
        if (CRC==0) and (S1[0]>0x80):
          #print("Reply from Addr=",S1[0]-0x80," data=",S[2:-2])
          RXdata=S[2:-2]   
          print("Reply from Addr=",S1[0]-0x80," data=",[hex(a) for a in RXdata])
        S=S[L+3:]
      if NoData: print("Communication error!")
      return RXdata;
    def GetID_Version(self):
      D1=self.RequestRegisters(HBA_VERSION,4);
      D2=self.RequestRegisters(HBA_ID,4);

      dt_string = datetime.now().strftime("%Y/%m/%d %H:%M")
      self.HWID=hextostr(D2);
      self.SWID=hextostr(reversed(D1));
      S="ID=0x"+self.HWID+" VERSION=0x"+self.SWID+" ("+dt_string+")";
      logging.info(S)
      return self.HWID,self.SWID;

    def powerOn(self):
        self.SetRegisters(HBA_PWR,[0x1F]);       
        self.beamform_on()
        self.set_BF()
        
    def beamform_on(self):
        self.SetRegisters(HBA_SWX,[8+6]);
        self.SetRegisters(HBA_SWY,[6]);

    def antenna_pwr_on(self):
        self.SetRegisters(HBA_FE_PWR,[0x0F]);
    def antenna_pwr_off(self):
        self.SetRegisters(HBA_FE_PWR,[0x00]);

    def one_antenna(self,chX=0,chY=0):
        Chs=[0,4,1,2]
        self.SetRegisters(HBA_SWX,[Chs[chX]]);
        self.SetRegisters(HBA_SWY,[Chs[chY]]);
        self.set_BF()

    def set_BF(self,G1=0,G2=0):
        if isinstance(G1,int): G1=[G1]*8
        if isinstance(G2,int): G2=[G2]*2
        for x in range(8): G1[x]=G1[x]&0xf
        for x in range(2): G2[x]=G2[x]&0xf
        G1[0]+=G2[0]<<4;   
        G1[4]+=G2[1]<<4;   
        logging.debug("Set BF: 0x"+hextostr(G1))
        self.SetRegisters(HBA_X,G1);    

    def SetAddress(self,HBA_select_new):
        logging.info("change address to"+str(HBA_select_new))
        self.SetRegisters(HBA_ADDR,[HBA_select_new]);#Change address
        self.HBA_select=HBA_select_new
        self.GetPackets();
        print(self.RequestRegisters(HBA_ADDR,1));
        self.GetPackets();        
        
    def SaveEEPROM(self):
        self.RequestRegisters(HBA_SAVE_EEPROM,1);
# def HBAbf(pol,ch,delay_group1,delay_group2,mode):
# """
# This function sets the delay for X or Y polarization for a 
# polarization pol => x, y
# specific channel ch ==>(1:4),
# delay in ps for group 1 (0..5400ps, 360ps step), and group 2 (0..10800ps, 720ps step),
# mode is controls a switch X,Y(1:4) single element(bit1:4) of beamformed (bit 5) set to 1.
# Example: HBAbf(X,1,1440,0,SINGLE)
# """


def bfswitch(ser,pol,ch):
#https://support.astron.nl/confluence/display/RCU/HBAT+Registers
    if pol[0].upper() == 'X':
     reg='0x08'
    elif pol[0].upper() == 'Y':
     reg='0x09'
    else:
        print('invalid value')

    if ch[0:1].upper() == 'CH1':
        swa_row = switch[switch['ch'].str.contains('CH1')]
        swdata = swa_row['DATA'].values[0]
    elif ch[0:1].upper() == 'CH2':
        swa_row = switch[switch['ch'].str.contains('CH2')]
        swdata = swa_row['DATA'].values[0]
    elif ch[0:1].upper() == 'CH3':
        swa_row = switch[switch['ch'].str.contains('CH3')]
        swdata = swa_row['DATA'].values[0]
    elif ch[0:1].upper() == 'CH4':
        swa_row = switch[switch['ch'].str.contains('CH4')]
        swdata = swa_row['DATA'].values[0]
    else:   print('invalid input value') 


def HBAbf_group1(ser,x1,x2,x3,x4,y1,y2,y3,y4):
    """
    This function sets the delay for X or Y polarization for a 
    ch is a matrix of 8
    delay in ps for group 1 (0..5400ps, 360ps step)
    """
    group1_data_mask = (1 << 0)|(1 << 1)|(1 << 2)|(1 << 3)
    
    import pandas as pd
    #data can be maintained in hbabf_csv files.
    #Read in the csv files register and datafiles into different lists
    group1 = pd.read_csv("group1.csv",sep=';',skipinitialspace = True)
    #group2 = pd.read_csv("group2.csv",sep=';',skipinitialspace = True)
    #switch = pd.read_csv("switch.csv",sep=';',skipinitialspace = True)
    registers = pd.read_csv("registers2.csv",sep=';',skipinitialspace = True)
    print(group1)
    #print(group2)
    #print(switch)
    print(registers)
    #Find for every attribute the value and register
    #ch attribute
    #Retrieve group1 register from ch, put the register in ch_reg variable

    #Retrieve delay data hex value from group2.csv, put the datavalue in delay_data(str)
    #find the correct line
    def delay2data(delay):
        qry = 'delay==' + str(delay)
        s = group1.query(qry)['data']
        if s.empty:
            print('not valid delay')
        return
    return group1.query(qry)['reg']
    
    regb = registers[registers['ch'].str.contains('X1')]
    sw_reg = regb['reg'].values[0]

    dx1 = hex(int(delay2data(x1)[0],16)) & group1_data_mask
    dx2 = hex(int(delay2data(x2)[0],16)) & group1_data_mask
    dx3 = hex(int(delay2data(x3)[0],16)) & group1_data_mask
    dx4 = hex(int(delay2data(x4)[0],16)) & group1_data_mask
    dy1 = hex(int(delay2data(y1)[0],16)) & group1_data_mask
    dy2 = hex(int(delay2data(y2)[0],16)) & group1_data_mask
    dy3 = hex(int(delay2data(y3)[0],16)) & group1_data_mask
    dy4 = hex(int(delay2data(y4)[0],16)) & group1_data_mask
    
    print(sw_reg, dx1, dx2, dx3, dx4, dy1, dy2, dy3, dy4 )
    
    SetRegisters(sw_reg, dx1, dx2, dx3, dx4, dy1, dy2, dy3, dy4 )
    #retreive crc, no checking only printing for now
    GetPackets();

    
def HBAbf_group2(ser,pol,delay):
    """
    This function sets the delay for X or Y polarization for a 
    polarization pol => x, y for group2
    group 2 (0..10800ps, 720ps step),
    Example: HBAbf_group2(X,1440)
    """
    group2_data_mask = (1 << 4)|(1 << 5)|(1 << 6)|(1 << 7)
    
    #import pandas as pd
    #data can be maintained in hbabf_csv files.
    #Read in the csv files register and datafiles into different lists
    #group1 = pd.read_csv("group1.csv",sep=';',skipinitialspace = True)
    group2 = pd.read_csv("group2.csv",sep=';',skipinitialspace = True)
    #switch = pd.read_csv("switch.csv",sep=';',skipinitialspace = True)
    registers = pd.read_csv("registers2.csv",sep=';',skipinitialspace = True)
    #print(group1)
    print(group2)
    #print(switch)
    print(registers)
    #Find for every attribute the value and register
    #pol attribute
    #register2 labels ch;reg;title;a;b;c;d

    #Retrieve group2 register from pol, put the register in pol_reg variable
    if pol[0].upper() == 'X':
        reg_row = registers[registers['ch'].str.contains('X1')]
        pol_reg = reg_row['reg'].values[0]
    elif pol[0].upper() == 'Y':
        reg_row = registers[registers['ch'].str.contains('Y1')]
        pol_reg = reg_row['reg'].values[0]
    else:   print('invalid input value') 
        
    #Retrieve delay data hex value from group2.csv, put the datavalue in delay_data(str)
    #find the correct line
    qry = 'delay==' + str(delay)
    s = group2.query(qry)['reg']
    if s.empty:
        print('not valid delay')
        return
    delay_data = group2.query(qry)['reg']
    print(pol_reg, hex(int(delay_data.values[0],16)))
    
    #make send command in right format and mask it
    delay_data_m = hex(int(delay_data.values[0],16)) & group2_data_mask 
    SetRegisters(pol_reg, delay_data_m )

    #retreive crc, no checking only printing for now
    GetPackets();
    



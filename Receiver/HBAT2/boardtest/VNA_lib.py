#!/usr/bin/env python
from rohdeschwarz import *
from rohdeschwarz.instruments.vna import *
import logging

def valid_ip(address):
    import socket
    try: 
        socket.inet_aton(address)
        return True
    except:
        return False

class vna:
        
    def __init__(self,ip='ZNB8-44-103249.nfra.nl'):
    #"""
    #This function sets up the LXI / ethernet connection and loads all modules
    #"""
    #import sys
   
    #if __name__ != "__main__":
    #    print("'{0}'\nis a script. Do not import!".format(__file__))
    #    print('Exiting...')
    #    sys.exit()

        self.vna = Vna()
   
       # if valid_ip(ip):
        self.vna.open_tcp(ip, 5025)
        self.vna.timeout=10000;
        #else:  
        #  print('IP length is not ok')
        #  return 
    
    #Turn on LOG file
        self.vna.open_log('SCPI Command Log.txt')
        logging.info(self.vna.id_string())
        print('Init done!')
#        return vna
    

    def sparam(self,nm,Channels=[1,2],verbose=False):
    #"""
    #This function measures S21 and saves it into a fname on the vna in path.
    #"""
    #vna is object, nm is path to measurement fil
    
        self.vna.channel(1).measure(Channels)
    # save the file of channel 1, where, port 1, port 2, format (re,im)
    # We use rawstrings
        rnm = r"{}".format(nm)
    #raw_fn = r"{}".format(fn)    
    #nm = raw_path + raw_fn
        self.vna.channel(1).save_measurement(rnm, Channels)
        if verbose: print(nm , "Sparam done!")

    def download(self,source,destiny,verbose=False):
    #"""
    #This function downloads the file from vna instrument to  the local computer
    #"""
        raw_s = r"{}".format(source)
        raw_d = r"{}".format(destiny)    
        logging.info("Download:"+str(source)+"->"+str(destiny))
        self.vna.file.download_file(raw_s, raw_d)
        if verbose: print("Download done!")


#Testing
#vna.channel(1).measure([1,2])
#vna.channel(1).save_measurement("nm", [2,1,])
#sparam('c:\\Users\\Public\\','j')


import logging

from pathlib import Path
import sys
if (len(sys.argv)<5) or (sys.argv[3] not in ['X','Y']) or (sys.argv[4] not in ['123','124','13','24']):
  print("required boardnr boardaddr X/Y 123/124/13/24");
  exit();

IPvna='ZNB8-44-103249.nfra.nl'
Channels=[1,2,3,4]
sf=Path('C:\\Users\\Public\\test2.s4p')
#destiny=Path('/home/kruger/git/hbat2/bf_lab_measure/meas2021_6_23/sparam')
#SETUP Directory here, every time!!!!!!
destiny=Path('/home/lofar4sw/bf_lab_measure/meas2021_6_24/sparam')
#sf=source / 'test2.s2p'
Board=int(sys.argv[1])
BoardAddr=int(sys.argv[2])
Pol=sys.argv[3]  #'X' or 'Y' depending on side
Inputs=sys.argv[4] #'123' #Input to which analyser port 1..3 is connected to. Other input terminated 75R

from platform import python_version
print(python_version())



logfile=destiny / ('board'+str(Board)+'.log')

logging.basicConfig(
                    handlers=[logging.FileHandler(logfile,'w', 'utf-8')], 
                    format='[%(levelname)-8s,%(filename)-20s] %(message)s',
                    level=logging.INFO)
                    
from VNA_lib import *
vna1 = vna(IPvna)



#INIT BOARD
from HBA_LIB import *
BF=HBAbf(port='/dev/ttyACM0',select=BoardAddr) #default=15, should be changed before putting in tile
#BF=HBAbf(port='/dev/ttyACM1',select=BoardAddr) #default=15, should be changed before putting in tile
print(BF.GetID_Version())
BF.powerOn()


#BF.beamform_on()
#BF.set_BF(0x00,0x00)




#Download group 1
if True:
    print("Group 1")
    G2=0 #Group to value for test
    logging.info("Group1 VNA test, Inputs="+Inputs+" Pol="+Pol+" Group2="+str(G2))
    for G1 in range(0,16,1):
        print(G1)
        BF.set_BF(G1,G2)
        vna1.sparam(sf,Channels)
        df=destiny / ('B%i%s_Ch%s_D%x%x.s4p' % (Board,Pol,Inputs,G2,G1))
        vna1.download(sf, df)



#Direct paths
if True:
    print("Direct path")
    BF.set_BF(0,0)
    for X in Inputs: #range(3):
        ch=int(X)-1;
        print(ch)
        BF.one_antenna(ch,ch)
        vna1.sparam(sf,Channels)
        df=destiny / ('B%i%s%i_Ch%s_D00.s4p' % (Board,Pol,ch+1,Inputs))
        vna1.download(sf, df)


#if '3' in Inputs:
if True:
    print("Group 2")
    #Test BF group 2
    G1=0 #Group to value for test
    #Pol='X' #used in filename
    #Inputs='13' #Input to which analyser port 1..3 is connected to. Other input terminated 75R
    BF.beamform_on()
    for G2 in range(0,16,1):
        print(G2)
        BF.set_BF(G1,G2)
        vna1.sparam(sf,Channels)
        df=destiny / ('B%i%s_Ch%s_D%x%x.s4p' % (Board,Pol,Inputs,G2,G1))
        vna1.download(sf, df)

exit()
# ## beam 1 boards: Analyser connected to inputs 1,2,4 and output

# In[ ]:



Inputs='124' #Input to which analyser port 1..3 is connected to. Other input terminated 75R


# In[12]:


#Test BF group 1
G2=0 #Group to value for test
#Inputs='124' #Input to which analyser port 1..3 is connected to. Other input terminated 75R
logging.info("Group1 VNA test, Inputs="+Inputs+" Pol="+Pol+" Group2="+str(G2))
BF.beamform_on()
for G1 in range(0,16,1):
    print(G1)
    BF.set_BF(G1,G2)
    vna1.sparam(sf,Channels)
    df=destiny / ('B%i%s_Ch%s_D%x%x.s4p' % (Board,Pol,Inputs,G2,G1))
    vna1.download(sf, df)


# In[13]:


#Direct path 4
BF.set_BF(0,0)
for ch in [3]:
    print(ch)
    BF.one_antenna(ch,ch)
    vna1.sparam(sf,Channels)
    df=destiny / ('B%i%s%i_Ch%s_D00.s4p' % (Board,Pol,ch+1,Inputs))
    vna1.download(sf, df)


# In[8]:


#Test BF group 1
#G2=0 #Group to value for test
#Pol='X' #used in filename
#Inputs='124' #Input to which analyser port 1..3 is connected to. Other input terminated 75R
#logging.info("Group1 VNA test, Inputs="+Inputs+" Pol="+Pol+" Group2="+str(G2))
#for G1 in range(0,16,1):
#    BF.set_BF(G1,G2)
#    vna1.sparam(sf,Channels)
#    df=destiny / ('B%i%s_Ch%s_D%x%x.s4p' % (Board,Pol,Inputs,G2,G1))
#    vna1.download(sf, df)


# In[10]:


#playing around
#G1=0 #Group to value for test
#Pol='X' #used in filename
#Inputs='123' #Input to which analyser port 1..3 is connected to. Other input terminated 75R
#for G2 in range(0,16,1):
#    BF.set_BF(G1,G2)
#    vna1.sparam(sf,Channels)
#    df=destiny / ('B%i%s_Ch%s_D%x%x.s4p' % (Board,Pol,Inputs,G2,G1))
#    vna1.download(sf, df)


# In[11]:


print('Done')


# ## beam 2 boards: Analyser connected to inputs 1,3 and output

# In[ ]:


Pol='Y' #used in filename
Inputs='13' #Input to which analyser port 1..3 is connected to. Other input terminated 75R


# In[15]:


#Test BF group 1
G2=0 #Group to value for test
logging.info("Group1 VNA test, Inputs="+Inputs+" Pol="+Pol+" Group2="+str(G2))
for G1 in range(0,16,1):
    print(G1)
    BF.set_BF(G1,G2)
    vna1.sparam(sf,Channels)
    df=destiny / ('B%i%s_Ch%s_D%x%x.s4p' % (Board,Pol,Inputs,G2,G1))
    vna1.download(sf, df)


# In[16]:


BF.set_BF(0,0)
for ch in [0,2]: #range(3):
    print(ch)
    BF.one_antenna(ch,ch)
    vna1.sparam(sf,Channels)
    df=destiny / ('B%i%s%i_Ch%s_D00.s4p' % (Board,Pol,ch+1,Inputs))
    vna1.download(sf, df)


# In[17]:


#Test BF group 2
G1=0 #Group to value for test
#Pol='X' #used in filename
#Inputs='13' #Input to which analyser port 1..3 is connected to. Other input terminated 75R
BF.beamform_on()
for G2 in range(0,16,1):
    print(G2)
    BF.set_BF(G1,G2)
    vna1.sparam(sf,Channels)
    df=destiny / ('B%i%s_Ch%s_D%x%x.s4p' % (Board,Pol,Inputs,G2,G1))
    vna1.download(sf, df)


# ## beam 2 boards: Analyser connected to inputs 2,4 and output

# In[ ]:


Inputs='24' 


# In[18]:


#Test BF group 1
G2=0 #Group to value for test
logging.info("Group1 VNA test, Inputs="+Inputs+" Pol="+Pol+" Group2="+str(G2))
BF.beamform_on()
for G1 in range(0,16,1):
    print(G1)
    BF.set_BF(G1,G2)
    vna1.sparam(sf,Channels)
    df=destiny / ('B%i%s_Ch%s_D%x%x.s4p' % (Board,Pol,Inputs,G2,G1))
    vna1.download(sf, df)


# In[19]:


BF.set_BF(0,0)
for ch in [1,3]:
    print(ch)
    BF.one_antenna(ch,ch)
    vna1.sparam(sf,Channels)
    df=destiny / ('B%i%s%i_Ch%s_D00.s4p' % (Board,Pol,ch+1,Inputs))
    vna1.download(sf, df)


# ## Change address
# 1) Change adress in microcontroller
# 
# 2) Store conf in (I2C) memory

# In[5]:


BF.SetAddress(1)  #Todo: this gie some communicaition error, but still we get a replay from new address


# In[6]:


BF.SaveEEPROM()


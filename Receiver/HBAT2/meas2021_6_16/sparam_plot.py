import skrf as rf
import numpy as np
import matplotlib.pyplot as plt
def dB2P(x):  return 10**(x/10);
def dBm2P(x): return 10**(x/10-3);
def P2dB(x):  return 10*np.log10(x);
def S2dB(x):  return 20*np.log10(np.abs(x));
def P2dBm(x): return 10*np.log10(x)+30;


boardnr=0
BoardDelayX=15.0e-9;
BoardDelayY=15.15e-9
D0G1=363e-12
D0G2=730e-12



def fn(path='sparam/',pol='X',Chs='123',G1=0,G2=0):
    s=path+('B%i%s_Ch%s_D%x%x.s4p' % (boardnr,pol,Chs,G2,G1))
    return s



def GetS21(ch=0,pol='X',G1=0,G2=0):
    if ch==3:
        return rf.Network(fn(G1=G1,G2=G2,pol=pol,Chs='124')).s[:,3,2]
    else:
        return rf.Network(fn(G1=G1,G2=G2,pol=pol,Chs='123')).s[:,3,ch]


def S75(S):
    Z=50*(1+S)/(1-S)/75    
    return (Z-1)/(Z+1)

def GetS11(ch=0,pol='X',G1=0,G2=0):
    if ch==3:
        return S75(rf.Network(fn(G1=G1,G2=G2,pol=pol,Chs='124')).s[:,2,2])
    else:
        return S75(rf.Network(fn(G1=G1,G2=G2,pol=pol,Chs='123')).s[:,ch,ch])
def GetS22(pol='X',G1=0,G2=0):
    return S75(rf.Network(fn(G1=G1,G2=G2,pol=pol,Chs='123')).s[:,3,3])


def SetBoard(boardnr1):
 global boardnr,freq
 boardnr=boardnr1
 S=rf.Network(fn())
 freq=S.f



biasT=rf.Network('sparam/YBiasT.s3p').s[:,1,0]
biasTloss=S2dB(biasT)
#biasphase=np.angle(S2dB(biasT)()
#plt.plot(biasTloss)




def PlotGain():
    plt.figure(dpi=300)
    GE=[]
    for pol in ['X','Y']:
       for ch in range(4):
          S21r=P2dB(np.abs(GetS21(ch=ch,pol=pol)))*2
          if pol=='Y': S21r-=biasTloss;
          GE.append(S21r)
          plt.plot(freq/1e6,S21r,label=pol+('%i'%(ch+1)))
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("S21 (dB)")
    plt.title("Board %i 50R Gain (0 delay)" % (boardnr))
    plt.grid()
    return GE

def PlotGain1():
    PlotGain()
    plt.legend(ncol=2)
    plt.ylim(-90,10)
    plt.xlim(0,500)
    plt.savefig("B%i_gain.png"%boardnr)

def PlotGain2():
    GE=PlotGain()
    plt.plot(freq/1e6,10*np.std(GE,axis=0),'k-',label='10x std dev')
    plt.legend(ncol=2)
    plt.plot([110,270],[2,2],'r-',linewidth=2)

    plt.ylim(0,10)
    plt.xlim(100,300)
    plt.savefig("B%i_gain2.png"%boardnr)


# In[9]:


def plotPCBdelay():
    plt.figure(dpi=300)
    GE=[]
#BoardDelay=1e-9;
#A0=np.exp(2j*np.pi*freq*BoardDelay)
    fstep=freq[1]-freq[0]
    for pol in ['X','Y']:
       for ch in range(4):
          S21r=GetS21(ch=ch,pol=pol)
          if pol=='Y': S21r/=biasT;
          S21r=-np.angle(S21r[1:]/S21r[:-1])/2/np.pi/fstep
          GE.append(S21r)
          plt.plot(freq[1:]/1e6,S21r*1e9,label=pol+('%i'%(ch+1)))
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("delay (ns)")
    plt.title("Board %i PCB delay (BF=0)" % (boardnr))
    plt.grid()
    return GE

def plotPCBdelay1():
    GE=plotPCBdelay()
    plt.plot(freq[1:]/1e6,10*np.std(GE,axis=0)*1e9,'k-',label='10x std dev')
    plt.legend(ncol=2)
    plt.ylim(0,25)
    plt.xlim(100,300)
    plt.savefig("B%i_delay.png"%boardnr)
    


# In[10]:


def plotPhaseError():
    plt.figure(dpi=300)
    GE=[]
    A0=np.exp(-2j*np.pi*freq*BoardDelayX)
    biasTdelay=np.exp(-2j*np.pi*freq*(BoardDelayY-BoardDelayX))
    fstep=freq[1]-freq[0]
    for pol in ['X','Y']:
       for ch in range(4):
          S21r=GetS21(ch=ch,pol=pol)
          if pol=='Y': S21r/=biasTdelay;       
          S21r=-np.angle(S21r/A0)/np.pi*180
          S21r+=360*(S21r<0)
#      if pol=='Y': S21r-=biasTloss;
          GE.append(S21r)
          plt.plot(freq/1e6,S21r,label=pol+('%i'%(ch+1)))
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("phase (degrees)")
    plt.title("Board %i phase (w.r.t %.2f,%.2f ns delay) (BF=0) " % (boardnr,BoardDelayX*1e9,BoardDelayY*1e9))
    plt.grid()
    plt.xlim(100,300)
    return GE

def plotPhaseError1():
    plotPhaseError()
    plt.legend(ncol=2)
    plt.ylim(200,250)
    plt.savefig("B%i_phase.png"%boardnr)

def plotPhaseError2():
    GE=plotPhaseError()
    plt.ylim(0,4)
    plt.plot(freq/1e6,np.std(GE,axis=0),'k-',label='std dev')
    plt.legend(ncol=2)
    plt.plot([110,270],[2,2],'r-',linewidth=2)
    plt.savefig("B%i_phase2.png"%boardnr)
    


# In[11]:


def G1gainDifference(ch=0,pol='X',doplot=False,group=1):
    GE=[]
    if group==2:
        S21r=GetS21(ch=ch,G2=0,pol=pol)
        for x in range(16):
            S21=GetS21(ch=ch,G2=x,pol=pol)
            GE.append(P2dB(np.abs(S21/S21r))*2)       
    else:
        S21r=GetS21(ch=ch,G1=0,pol=pol)
        for x in range(16):
            S21=GetS21(ch=ch,G1=x,pol=pol)
            GE.append(P2dB(np.abs(S21/S21r))*2)

    GE=np.array(GE)
    GE-=np.mean(GE,axis=0)
    GEstd=np.std(GE,axis=0)
    if doplot:
        for x in range(16):
            plt.plot(freq/1e6,GE[x],('-' if x<8 else '--'),label=str(x))
        plt.plot(freq/1e6,GEstd,'k-',linewidth=2)
        plt.plot(freq/1e6,-GEstd,'k-',linewidth=2)
        plt.plot([110,270],[-0.2,-0.2],'r-',linewidth=2)
        plt.plot([110,270],[0.2,0.2],'r-',linewidth=2)
        plt.ylim(-1,1)
        plt.xlim(100,300)
        plt.legend(fontsize='small',ncol=4)
        plt.xlabel("Frequency (MHz)")
        plt.ylabel("Gain difference (dB)")
        plt.title("X1 Group 1 gain difference")
    return GEstd

#plt.figure(dpi=300)       
#G1gainDifference(doplot=True)
#plt.savefig("X1G1_gain_error.png")


# In[12]:


def plotGainDifference():
    plt.figure(dpi=300)       
    for Pol in ['X','Y']:
        line=('-' if Pol=='X' else '--')
        for x in range(4):
            GE=G1gainDifference(ch=x,pol=Pol)
            plt.plot(freq/1e6,GE,line,label=Pol+str(x+1))
        GE=G1gainDifference(ch=0,group=2,pol=Pol)
        plt.plot(freq/1e6,GE,line,label=Pol+'G2')
    plt.plot([110,270],[0.2,0.2],'r-',linewidth=2)
    plt.ylim(0,1)
    plt.xlim(100,300)
    plt.legend(fontsize='small',ncol=2)
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("Gain difference (dB)")
    plt.title("Board "+str(boardnr)+" gain std dev between delays")
    plt.savefig("B%i_gain_error.png"%boardnr)


# In[13]:


def G1phaseDifference(ch=0,pol='X',doplot=False,group=1,D0=363e-12):
    PE=[]
    S21r=GetS21(ch=ch,pol=pol)
    if group==2:
        for x in range(16):
            S21=GetS21(ch=ch,G2=x,pol=pol)
            A0=np.exp(2j*np.pi*freq*D0*x)
            PE.append((np.angle(S21r/S21/A0))/np.pi*180)      
    else:
        for x in range(16):
            S21=GetS21(ch=ch,G1=x,pol=pol)
            A0=np.exp(2j*np.pi*freq*D0*x)
            PE.append((np.angle(S21r/S21/A0))/np.pi*180)      

    PE=np.array(PE)
    PE-=np.mean(PE,axis=0)
    PEstd=np.std(PE,axis=0)
    if doplot:
        for x in range(16):
            plt.plot(freq/1e6,PE[x],('-' if x<8 else '--'),label=str(x))
        plt.plot(freq/1e6,PEstd,'k-',linewidth=2)
        plt.plot(freq/1e6,-PEstd,'k-',linewidth=2)
        plt.plot([110,270],[-2,-2],'r-',linewidth=2)
        plt.plot([110,270],[2,2],'r-',linewidth=2)
        plt.ylim(-6,6)
        plt.xlim(100,300)
        plt.legend(fontsize='small',ncol=4)
        plt.xlabel("Frequency (MHz)")
        plt.ylabel("Phase error (degrees)")
        plt.title("X2 Group 1 phase errors (Dref=%.1f ps)" % (D0*1e12))
    return PEstd

#plt.figure(dpi=300)       
#G1phaseDifference(doplot=True);
#plt.savefig("X1G1_gain_error.png")


# In[14]:


def plotPhaseDifference():
    plt.figure(dpi=300)       
    for Pol in ['X','Y']:
        line=('-' if Pol=='X' else '--')
        for x in range(4):
            GE=G1phaseDifference(ch=x,pol=Pol,D0=D0G1)
            plt.plot(freq/1e6,GE,line,label=Pol+str(x+1))
        GE=G1phaseDifference(ch=0,group=2,pol=Pol,D0=D0G2)
        plt.plot(freq/1e6,GE,line,label=Pol+'G2')
    plt.plot([110,270],[2,2],'r-',linewidth=2)
    plt.ylim(0,6)
    plt.xlim(100,300)
    plt.legend(fontsize='small',ncol=2)
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("Phase error (degrees)")
    plt.title("Board "+str(boardnr)+" phase std dev between delays (Step=%.1f,%.1f ps)" % (D0G1*1e12,D0G2*1e12))
    plt.savefig("B%i_phase_error.png"%boardnr)


# In[15]:


def G1gainSteps(ch=0,pol='X',doplot=[],group=1):
    GE=[]
    line=('-' if pol=='X' else '--')
    doplot[0,0].get_xaxis().set_visible(False)
    doplot[0,1].get_xaxis().set_visible(False)
    doplot[0,1].get_yaxis().set_visible(False)
    doplot[1,1].get_yaxis().set_visible(False)

    for x in range(16):
        if group==2:
            S21=GetS21(ch=ch,G2=x,pol=pol)
        else:
            S21=GetS21(ch=ch,G1=x,pol=pol)
        GE.append(P2dB(np.abs(S21))*2)
    for x in range(4):
        bit=1<<x;
        GE2=[]
        for y in range(16):
            y2=y|bit;
            if y==y2: continue
            diff=GE[y]-GE[y2]
            GE2.append(diff)

        GE2=np.array(GE2)
        GE2m=np.mean(GE2,axis=0)#/x
        GEstd=np.std(GE2,axis=0)
        if len(doplot)>0:
            y=(x+group-1)%4
            ax=doplot[y//2,y%2]
            ax.plot(freq/1e6,GE2m,line,label=("%s%i"%(pol,ch) if group==1 else "%sG2"%(pol)))
        #plt.plot(freq/1e6,-GEstd,'k-',linewidth=2)
        ##plt.plot([110,270],[-0.2,-0.2],'r-',linewidth=2)
        #plt.plot([110,270],[0.2,0.2],'r-',linewidth=2)
            ax.set_ylim(-0.4,0.6)
            ax.set_xlim(100,300)
        #plt.legend(fontsize='small',ncol=4)
        #plt.xlabel("Frequency (MHz)")
        #plt.ylabel("Gain difference (dB)")
            ax.set_title("delay %i"%y)
#    return GE2m
def DelayLineGainErrors():
    plt.figure(dpi=300)
    fig, axs = plt.subplots(2, 2,dpi=300)
    for ch in range(4):
        G1gainSteps(ch=ch,pol='X',doplot=axs,group=1);
    G1gainSteps(ch=0,pol='X',doplot=axs,group=2);
    for ch in range(4):
        G1gainSteps(ch=ch,pol='Y',doplot=axs,group=1);
    G1gainSteps(ch=0,pol='Y',doplot=axs,group=2);
    axs[0,0].legend(fontsize='x-small',ncol=4)
    axs[0,0].set_title("Board %i: delay 0/4"%boardnr)
    axs[0,0].set_ylabel("Gain difference Off-On (dB)")
    plt.savefig("B%i_gain_error_delays.png"%boardnr)


# In[16]:


def G1phaseSteps(ch=0,pol='X',doplot=[],group=1,D0=363e-12):
    GE=[]
    line=('-' if pol=='X' else '--')
    doplot[0,0].get_xaxis().set_visible(False)
    doplot[0,1].get_xaxis().set_visible(False)
    doplot[0,1].get_yaxis().set_visible(False)
    doplot[1,1].get_yaxis().set_visible(False)

          
    for x in range(16):
        if group==2:
            S21=GetS21(ch=ch,G2=x,pol=pol)
        else:
            S21=GetS21(ch=ch,G1=x,pol=pol)
        A0=np.exp(-2j*np.pi*freq*D0*x)
        GE.append(S21/A0)
    for x in range(4):
        bit=1<<x;
        GE2=[]
        for y in range(16):
            y2=y|bit;
            if y==y2: continue
            diff=np.angle(GE[y2]/GE[y])/np.pi*180
            GE2.append(diff)

        GE2=np.array(GE2)
        GE2m=np.mean(GE2,axis=0)#/x
        GEstd=np.std(GE2,axis=0)
        if len(doplot)>0:
            y=(x+group-1)%4
            ax=doplot[y//2,y%2]
            ax.plot(freq/1e6,GE2m,line,label=("%s%i"%(pol,ch) if group==1 else "%sG2"%(pol)))
        #plt.plot(freq/1e6,-GEstd,'k-',linewidth=2)
        ##plt.plot([110,270],[-0.2,-0.2],'r-',linewidth=2)
        #plt.plot([110,270],[0.2,0.2],'r-',linewidth=2)
            ax.set_ylim(-6,10)
            ax.set_xlim(100,300)
        #plt.legend(fontsize='small',ncol=4)
        #plt.xlabel("Frequency (MHz)")
        #plt.ylabel("Gain difference (dB)")
            ax.set_title("delay %i"%y)
#    return GE2m
def DelayLinePhaseErrors():
    plt.figure(dpi=300)
    fig, axs = plt.subplots(2, 2,dpi=300)
    for ch in range(4):
        G1phaseSteps(ch=ch,pol='X',doplot=axs,group=1,D0=363e-12);
    G1phaseSteps(ch=0,pol='X',doplot=axs,group=2,D0=370e-12*2);
    for ch in range(4):
        G1phaseSteps(ch=ch,pol='Y',doplot=axs,group=1,D0=363e-12);
    G1phaseSteps(ch=0,pol='Y',doplot=axs,group=2,D0=370e-12*2);
    axs[1,0].legend(fontsize='x-small',ncol=4)
    axs[0,0].set_title("Board %i: delay 0/4"%boardnr)
    axs[0,0].set_ylabel("Phase error (degrees)")
    plt.savefig("B%i_phase_error_delays.png"%boardnr)


# In[17]:


def plotS11():
    plt.figure(dpi=300)
    GE=[]
    for pol in ['X','Y']:
       for ch in range(4):
          S21r=S2dB(GetS11(ch=ch,pol=pol))
          GE.append(S21r)
          plt.plot(freq/1e6,S21r,label=pol+('%i'%(ch+1)))

    plt.legend(ncol=2)
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("S11 (dB)")
    plt.title("Board %i input reflection (75R, 0 delay)" % (boardnr))
    plt.xlim(100,300)
    plt.ylim(-30,0)
    plt.grid()
    plt.savefig("B%i_S11.png"%boardnr)


# In[18]:


def plotS22():
    plt.figure(dpi=300)
    for pol in ['X','Y']:
       line=('-' if pol=='X' else '--')
       S21r=S2dB(GetS22(pol=pol))
       plt.plot(freq/1e6,S21r,line,label='B%i %s'%(boardnr,pol))

    plt.plot([110,270],[-17,-17],'k-',linewidth=2)
    plt.legend(ncol=2)
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("S22 (dB)")
    plt.title("Output reflection (75R, 0 delay)" )
    plt.xlim(100,300)
    plt.ylim(-30,0)
    plt.grid()
    plt.savefig("S22.png");


